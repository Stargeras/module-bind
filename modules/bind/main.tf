resource "helm_release" "bind" {
  name             = "bind"
  namespace        = var.namespace
  chart            = "${path.module}/helm/bind"
  create_namespace = true

  values = [
    templatefile("${path.module}/values-tmpl.yaml", {
      service_type      = var.service_type
      domain_name       = var.domain_name
      domain_forwarders = var.domain_forwarders
      replicas          = var.replicas
      records           = var.records
      loadbalancer_ip   = var.loadbalancer_ip
      nodeport          = var.nodeport
    })
  ]
}