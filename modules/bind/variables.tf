variable "namespace" {}
variable "replicas" {}
variable "service_type" {}
variable "domain_name" {}
variable "domain_forwarders" {}
variable "records" {}
variable "loadbalancer_ip" {}
variable "nodeport" {}