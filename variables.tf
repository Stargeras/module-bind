variable "domain_name" {
  default     = "local.domain"
  description = "Domain name of DNS zone"
  type        = string
}
variable "domain_forwarders" {
  default     = "23.82.1.1"
  description = "Upstream DNS server to forward DNS queries that don't exist locally on the bind server"
  type        = string
}
variable "namespace" {
  type        = string
  default     = "bind"
  description = "Namespace to deploy bind helmchart into"
}
variable "replicas" {
  default     = 1
  description = "Number of bind replicas to create in deployment"
  type        = number
}
variable "service_type" {
  default     = "LoadBalancer"
  description = "Service type of bind service"
  type        = string
}
variable "loadbalancer_ip" {
  default     = ""
  description = "IP address to utilize if `service_type` is `LoadBalancer`. Leave blank to auto-assign"
  type        = string
}
variable "nodeport" {
  default     = ""
  description = "Port to use if `service_type` is `NodePort`"
  type        = string
}

variable "records" {
  type = map(object({
    name   = string
    type   = string
    target = string
  }))
  description = "Map of DNS records to create in bind server"
}

variable "kubernetes_auth_host" {
  description = "The hostname (in form of URI) of the Kubernetes API"
  type        = string
}
variable "kubernetes_auth_cluster_ca_certificate" {
  description = "PEM-encoded root certificates bundle for TLS authentication"
  type        = string
}
variable "kubernetes_auth_client_certificate" {
  default     = ""
  description = "PEM-encoded client certificate for TLS authentication"
  type        = string
}
variable "kubernetes_auth_client_key" {
  default     = ""
  description = "PEM-encoded client certificate key for TLS authentication"
  type        = string
}
variable "kubernetes_auth_token" {
  default     = ""
  description = "Service account token"
  type        = string
}
