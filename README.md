# Sourcing example
``` terraform
module "bind" {
  source                                 = "git::https://gitlab.com/Stargeras/module-bind"
  kubernetes_auth_host                   = module.kubernetes.kubernetes_auth_host
  kubernetes_auth_cluster_ca_certificate = module.kubernetes.kubernetes_auth_cluster_ca_certificate
  kubernetes_auth_client_certificate     = module.kubernetes.kubernetes_auth_client_certificate
  kubernetes_auth_client_key             = module.kubernetes.kubernetes_auth_client_key
  kubernetes_auth_token                  = module.kubernetes.kubernetes_auth_token
  domain_forwarders                      = "23.82.1.1"
  domain_name                            = "local.domain"
  records = {
    1 = {
      name   = "webapp"
      type   = "A"
      target = "23.82.1.70"
    }
  }
}
```
<!-- BEGIN_TF_DOCS -->
## Requirements

No requirements.

## Providers

No providers.

## Modules

| Name | Source | Version |
|------|--------|---------|
| <a name="module_bind"></a> [bind](#module\_bind) | ./modules/bind | n/a |

## Resources

No resources.

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_domain_forwarders"></a> [domain\_forwarders](#input\_domain\_forwarders) | Upstream DNS server to forward DNS queries that don't exist locally on the bind server | `string` | `"23.82.1.1"` | no |
| <a name="input_domain_name"></a> [domain\_name](#input\_domain\_name) | Domain name of DNS zone | `string` | `"local.domain"` | no |
| <a name="input_kubernetes_auth_client_certificate"></a> [kubernetes\_auth\_client\_certificate](#input\_kubernetes\_auth\_client\_certificate) | PEM-encoded client certificate for TLS authentication | `string` | `""` | no |
| <a name="input_kubernetes_auth_client_key"></a> [kubernetes\_auth\_client\_key](#input\_kubernetes\_auth\_client\_key) | PEM-encoded client certificate key for TLS authentication | `string` | `""` | no |
| <a name="input_kubernetes_auth_cluster_ca_certificate"></a> [kubernetes\_auth\_cluster\_ca\_certificate](#input\_kubernetes\_auth\_cluster\_ca\_certificate) | PEM-encoded root certificates bundle for TLS authentication | `string` | n/a | yes |
| <a name="input_kubernetes_auth_host"></a> [kubernetes\_auth\_host](#input\_kubernetes\_auth\_host) | The hostname (in form of URI) of the Kubernetes API | `string` | n/a | yes |
| <a name="input_kubernetes_auth_token"></a> [kubernetes\_auth\_token](#input\_kubernetes\_auth\_token) | Service account token | `string` | `""` | no |
| <a name="input_loadbalancer_ip"></a> [loadbalancer\_ip](#input\_loadbalancer\_ip) | IP address to utilize if `service_type` is `LoadBalancer`. Leave blank to auto-assign | `string` | `""` | no |
| <a name="input_namespace"></a> [namespace](#input\_namespace) | Namespace to deploy bind helmchart into | `string` | `"bind"` | no |
| <a name="input_nodeport"></a> [nodeport](#input\_nodeport) | Port to use if `service_type` is `NodePort` | `string` | `""` | no |
| <a name="input_records"></a> [records](#input\_records) | Map of DNS records to create in bind server | <pre>map(object({<br>    name   = string<br>    type   = string<br>    target = string<br>  }))</pre> | n/a | yes |
| <a name="input_replicas"></a> [replicas](#input\_replicas) | Number of bind replicas to create in deployment | `number` | `1` | no |
| <a name="input_service_type"></a> [service\_type](#input\_service\_type) | Service type of bind service | `string` | `"LoadBalancer"` | no |

## Outputs

No outputs.
<!-- END_TF_DOCS -->