module "bind" {
  source            = "./modules/bind"
  namespace         = var.namespace
  replicas          = var.replicas
  service_type      = var.service_type
  domain_name       = var.domain_name
  domain_forwarders = var.domain_forwarders
  records           = var.records
  loadbalancer_ip   = var.loadbalancer_ip
  nodeport          = var.nodeport
}